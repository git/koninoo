/*
 * This file is part of koninoo.
 *
 * koninoo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * koninoo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 iwakeh
 */
package org.ourproject.koninoo.protocol;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author iwakeh
 */
public class OnionooBase {

  protected final String RELPUB = "relays_published ";
  protected final String RELS = "relays: ";
  protected final String BRIPUB = "bridges_published ";
  protected final String BRIS = "bridges: ";
  private final String RE = "relays";
  private final String BR = "bridges";
  private final String NONE = "<none>";
  private final Logger log = Logger.getLogger(getClass().getName());

  private Object[] relays = null;
  private String relaysPublished;
  private Object[] bridges = null;
  private String bridgesPublished;
  private Base[] ir;
  private Base[] ib;

  public OnionooBase(String s) {
    readJson(s);
  }

  public OnionooBase(InputStream is) {
    readJson(new InputStreamReader(is));
  }

  private void readJson(String j) {
    readJson(new StringReader(j));
  }

  private void readJson(Reader reader) {
    JsonReader r = Json.createReader(reader);
    JsonObject o = r.readObject();
    setRelays_published(o.getJsonString(RELPUB.trim()));
    setBridges_published(o.getJsonString(BRIPUB.trim()));
    relays = o.getJsonArray(RE).toArray();
    bridges = o.getJsonArray(BR).toArray();
  }

  public Object[] getBridges() {
    return bridges;
  }

  public void setBridges(Object[] bridges) {
    this.bridges = bridges;
  }

  public Object getBridges_published() {
    return bridgesPublished;
  }

  public void setBridges_published(Object bp) {
    this.bridgesPublished = bp.toString().replaceAll("\"", "");
  }

  public Object getRelays_published() {
    return relaysPublished;
  }

  public void setRelays_published(Object rp) {
    this.relaysPublished = rp.toString().replaceAll("\"", "");
  }

  public Object[] getRelays() {
    return relays;
  }

  public void setRelays(Object[] relays) {
    this.relays = relays;
  }
  private boolean processed;
  private String myString;

  @Override
  public String toString() {
    if (!processed) {
      constructAll();
      myString = RELPUB + getRelays_published() + '\n' + RELS
          + constructFrom(ir)
          + BRIPUB + getBridges_published() + '\n' + BRIS
          + constructFrom(ib);
    }
    return myString;
  }

  private void constructAll() {
    ir = new Base[getRelays().length];
    for (int i = 0; i < relays.length; i++) {
      ir[i] = new Base(relays[i]);
    }
    ib = new Base[getBridges().length];
    for (int i = 0; i < bridges.length; i++) {
      ib[i] = new Base(bridges[i]);
    }
  }

  private String constructFrom(Base[] b) {
    if (null == b || b.length == 0) {
      return NONE;
    } else {
      StringBuilder sb = new StringBuilder("\n");
      for (int i = 0; i < b.length; i++) {
        sb.append(b[i].toString().replaceAll("\"", "")).append("\n");
      }
      return sb.toString();
    }
  }

}
