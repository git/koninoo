/*
 * /*
 * This file is part of koninoo.
 * 
 * koninoo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * koninoo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 *  along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2014 iwakeh
 */
package org.ourproject.koninoo.protocol;

import java.util.Map;

/**
 *
 * @author iwakeh
 */
public class Base {

  Map<String, Object> mapping;
  private final String my;

  Base(Object o) {
    mapping = (Map<String, Object>) o;
    if (null != mapping && !mapping.isEmpty()) {
      StringBuilder sb = new StringBuilder("\n");
      for (Map.Entry<String, Object> ent : mapping.entrySet()) {
        sb.append(ent.getKey() + ": " + ent.getValue()).append("\n");
      }
      my = sb.toString();
    } else {
      my = "";
    }
  }

  @Override
  public String toString() {
    return my;
  }

}
