/*
 * This file is part of koninoo.
 *
 * koninoo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * koninoo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 iwakeh
 */
package org.ourproject.koninoo.protocol;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.ourproject.koninoo.Koni.display;
import static org.ourproject.koninoo.Koni.display;

public class Kurl extends HashMap<String, String> {

  private Logger log = Logger.getLogger(getClass().getName());
  static String ERRMARKER = "=>---> ";
  private String serviceprefix = "summary";
  public static String OK = "vale";
  public static String BASEURL = "https://onionoo.torproject.org/";

  public Kurl() {
    this(BASEURL);
  }

  public Kurl(String url) {
    BASEURL = url;
    put("limit", "4");
    log.info("Using " + BASEURL);
  }

  public URL getUrl() {
    try {
      return new URL(BASEURL + serviceprefix + getParamString());
    } catch (MalformedURLException ex) {
      log.log(Level.SEVERE, ex.getMessage(), ex);
    }
    return null;
  }

  private String getParamString() {

    StringBuilder sb = new StringBuilder("?");
    for (Entry<String, String> ent : entrySet()) {
      sb.append(ent.getKey()).append("=").append(ent.getValue()).append("&");
    }
    return sb.substring(0, sb.length() - 1);
  }

  public void setService(String svc) {
    List<Services> s = new ArrayList<>();
    for (Services t : Services.values()) {
      if (t.toString().startsWith(svc.toLowerCase())) {
        s.add(t);
      }
    }
    if (s.isEmpty()) {
      handleError("Unknown service. Choose from " + Arrays.toString(Services.values()));
    } else if (s.size() > 1) {
      handleError("Ambiguous service. Choose from " + Arrays.toString(Services.values()));
    } else {
      serviceprefix = s.get(0).toString();
    }
  }

  public void setParam(String in) {
    String[] s = in.trim().toLowerCase().split("=");
    if (s.length < 2) {
      remove(s[0]);
    } else if (!validParams.containsKey(s[0])) {
      handleError("Unknown parameter. Choose from: " + validParams.keySet());
    } else if (validParams.get(s[0]).isEmpty() || validParams.get(s[0]).contains(s[1])) {
      put(s[0], s[1]);
    } else {
      handleError("Unknown value. Choose from: " + validParams.get(s[0]));
    }
  }

  public static void handleError(String erg) {
    if (!Kurl.OK.equals(erg)) {
      display(ERRMARKER + erg, true);
    }
  }
  private final static Map<String, Set<String>> validParams = new HashMap<String, Set<String>>();
  private final static String[] typen = new String[]{"bridge", "relay"};
  private final static String[] running = new String[]{"true", "false"};
  private final static String[] empty = new String[]{"type", "running", "order", "offset", "limit",
    "fields", "lookup", "fingerprint", "country", "as", "flag", "first_seen_days",
    "last_seen_days", "contact", "family"};
  private final static String[] ordering = new String[]{"consensus_weight", "-consensus_weight"};

  static {
    for (String s : empty) {
      validParams.put(s, Collections.EMPTY_SET);
    }
    validParams.put("type", new HashSet<>(Arrays.asList(typen)));
    validParams.put("order", new HashSet<>(Arrays.asList(ordering)));
    validParams.put("running", new HashSet<>(Arrays.asList(running)));
  }

  public String getService() {
    return serviceprefix;
  }
}
