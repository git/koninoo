/*
 * This file is part of koninoo.
 *
 * koninoo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * koninoo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 iwakeh
 */
package org.ourproject.koninoo;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;
import static org.ourproject.koninoo.Koni.display;
import org.ourproject.koninoo.protocol.Kurl;
import org.ourproject.koninoo.protocol.OnionooBase;

/**
 *
 * @author iwakeh
 */
public final class Processor {

  private static final Logger log = Logger.getLogger("koninoo");
  public static boolean debug;

  private Processor() {
  }

  public static void search(String q, Kurl kurl) {
    if (null != q && q.length() > 0) {
      kurl.put("search", q);
    }
    String aus = "";
    URL url = null;
    url = kurl.getUrl();
    aus = parseToString(url);
    display(aus, true);
  }

  private static void handleProblem(IOException ex) {
    String m = ex.getMessage();
    String rep = "Unknown error.";
    int httpIndex = m.indexOf("HTTP response code: ");
    if (httpIndex > -1) {
      String h = m.substring(httpIndex, httpIndex + 3);
      if (null != h) {
        switch (h) {
          case "400":
          case "404"://Not Available
            rep = "HTTP 400: Bad request.";
            break;
          case "500":
            rep = "HTTP 500: Internal Server Error.";
            break;
          case "503":
            rep = "HTTP 503: Service Unavailable.";
        }
      }
    }
    display(rep + "\n" + ex.getMessage(), true);
  }

  public static String parseToString(URL url) {
    OnionooBase k = null;
    try {
      Class clazz = OnionooBase.class;
      k = new OnionooBase(url.openStream());
    } catch (IOException ex) {
      handleProblem(ex);
      return "";
    }
    return k.toString();
  }

}
