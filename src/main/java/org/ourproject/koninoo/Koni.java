/*
 * This file is part of koninoo.
 *
 * koninoo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * koninoo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 iwakeh
 */
package org.ourproject.koninoo;

import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ourproject.koninoo.protocol.Kurl;
import org.ourproject.koninoo.protocol.Services;

/**
 *
 * @author iwakeh
 */
public final class Koni {

  private static Logger log = Logger.getLogger("koninoo");
  static String INTRO_TEXT
      = "################################\n"
      + "###### welcome to koninoo ######\n"
      + "###### explore tor stats  ######\n"
      + "################################\n"
      + "enter   #help     for more info.";
  private static String USAGE = "  #quit - closes this application.\n"
      + "Usage: \n"
      + " <any text or nothing> and <return> will display matching entries\n"
      + " #<format name>  and <return> changes the retrieval format.\n"
      + " List of possible formats:\n" + Arrays.toString(Services.values()) + "\n"
      + " Parameters can be set by: \n"
      + "  <parameter name>=<parameter value>\n"
      + " an empty value unsets the parameter.\n"
      + "\nMore about the Onionoo protocol can be found here:\n "
      + Kurl.BASEURL + "\n";
  private static int limit = 3;
  private static String prefix = "summary";
  private static boolean rawOutput = false;
  private Kurl kurl = null;
  private static String prompt = "k> ";
  private static Console c;
  private static PrintWriter cout;
  private static final String SETTER = "=";
  private static final String COMMAND = "#";

  private Koni(String[] a) {
    if (null == a || a.length != 1) {
      kurl = new Kurl();
      return;
    }
    try {
      URL u = new URL(a[0]);
      kurl = new Kurl(a[0]);
    } catch (MalformedURLException ex) {
      log.log(Level.SEVERE, ex.getMessage());
      kurl = new Kurl();
    }
  }

  public static void main(String[] a) throws MalformedURLException, IOException {
    Koni yo = new Koni(a);
    c = System.console();
    if (c == null) {
      log.warning("This application should be run from a command line or terminal.\n\t"
          + "e.g.:\t  java -jar koninoo-1.0.jar");
      System.exit(1);
    }
    cout = c.writer();

    display(cout, INTRO_TEXT, true);
    String in;
    do {
      yo.getKurl().remove("search");
      in = c.readLine(prompt);
      if (in.contains(SETTER)) {
        yo.getKurl().setParam(in);
      } else if (in.startsWith(COMMAND)) {
        switch (in.toLowerCase().substring(1)) {
          case "debug":
            Processor.debug = !Processor.debug;
            break;
          case "quit":
          case "exit":
          case "bye":
            cout.append("Bye, bye.\n").flush();
            return;
          case "h":
          case "help":
          case "?":
            display(cout, USAGE, false);
            break;
          case "raw":
            rawOutput = !rawOutput;
            break;

          default:
            yo.getKurl().setService(in.toLowerCase().substring(1));
        }
      } else {
        Processor.search(in, yo.getKurl());
      }
    } while (true);
  }

  public static void display(PrintWriter w, String text, boolean newline) {
    if (newline) {
      w.append(text + "\n").flush();
    } else {
      w.append(text).flush();
    }
  }

  public static void display(String text, boolean newline) {
    if (cout != null) {
      display(cout, text, newline);
    } else {
      display(new PrintWriter(System.out), text, newline);
    }
  }

  public Kurl getKurl() {
    return kurl;
  }

}
