######################################################################
#
# This file is part of koninoo.
#
# koninoo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# koninoo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koninoo.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014 iwakeh
#######################################################################

Requires: Java 1.7 or above

Running koninoo:
This application should be run from a command line or terminal.
e.g.:

  java -jar koninoo-cli-<version>.jar [onionoo server]

The command line argument should be a server url, e.g. the http address
of your local Onionoo server: http://localhost:8080/onionoo
This url is optional. If omitted, the Tor project's Onionoo server will
be queried. 

Usage: 
 <any text or nothing> and <return> will display matching entries
 #<format name>   and <return> changes the retrieval format.
 List of possible formats:
  summary, details, bandwidth, uptime, weights, clients
 Parameters can be set by: 
  <parameter name>=<parameter value>
  an empty value unsets the parameter.

  #quit - closes koninoo.

A detailed description, news about koninoo, and the bug tracker are 
available here:
https://savannah.nongnu.org/projects/koninoo
 
More about the Onionoo protocol can be found here:
 https://onionoo.torproject.org/protocol.html
